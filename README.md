# UCSM Script


UCSM Script es un pequeño proyecto creado con el proposito de obtener la lista de todos los cursos de las diferentes carreras y niveles impartidos en la Universidad Catolica de Santa Maria.

### Contexto

Actualmente para buscar un curso en particular, debes seguir los siguiente pasos:


  - Hacer login en la web de la UCSM
  - Dirigirte a la pestana - Consultas > Horario General (recarga)
  - Elegir una escuela profesional (carga)
  - Elegir un semestre (carga)
  - Elegir un curso del semestre seleccionado previamente (carga y finalmente te muestra la informacion del curso como grupos, docente, etc)


La universidad te da la oportunidad de llevar cualquier curso que se convalide con el tuyo en una diferente carrera haciendo el tramite correspondiente, por ende, si quisieras buscar un curso en otra carrera tendria que realizar los pasos anteriormente descritos una y otra vez manualemte hasta que des con el que mas te agrade.
No me agrado la idea de realizar todo esto manualemte asi que cree UCSM Script para facilitarme esta tarea.
Realizando scraping a la web de la UCSM obtengo todas las carreras, semetres, cursos que estan impartiendo. Asi facilmente usando la herramienta de busqueda del navegador puedo buscar el curso que deseo.


### Tecnología

UCSM Script usa unicamente:

* [PHP]

### Instalación

UCSM Script requiere minimamente un servidor [APACHE] y [PHP] v7+ para correr.

Basta con clonar el proyecto dentro de una carpeta su servidor [APACHE].

```sh
$ git clone https://gitlab.com/7CHETOS/ucsm-scraper.git
$ unzip ucsm-scraper.zip
$ cd ucsm-scraper
```

Luego de levantar su servidor [APACHE] con [PHP] puede revisar el proyecto en el navegador de su preferencia.

```sh
127.0.0.1/ucsm
```
### Video
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/0_GnYotm_J0/0.jpg)](https://www.youtube.com/watch?v=0_GnYotm_J0)
