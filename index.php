<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
    <section>
        <?php
            require_once("controller/UniversityController.php");
            $app1 = new UniversityController();
            // $career = $app1->index();
            // $semester = $app1->index2();
            // $course = $app1->index3();
            // $course = array_unique($course->ObtenerCursosSemestreProgramaResult, SORT_REGULAR);
            // $schedule = $app1->index4();
            $data = $app1->getCourse();
        ?>
        <div class="container">

            <div class="row">
                <?php if(isset($data)): ?>
                    <table class="table table-sm table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Carrera</th>
                                <th scope="col">Semestre</th>
                                <th scope="col">Curso</th>
                                <th scope="col">Docente</th>
                                <th scope="col">Dia</th>
                                <th scope="col">Hora</th>
                                <th scope="col">Info</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($data as $key => $value): ?>
                                <tr>
                                    <th scope="row"> <?=$key+1;?> </th>
                                    <td> <?=$value["career"];?> </td>
                                    <td> <?=$value["semester"];?> </td>
                                    <td> <?=$value["course"];?> </td>

                                    <td> <?=$value["teacher"];?> </td>
                                    <td> <?=$value["day"];?> </td>
                                    <td> <?=$value["hour"];?> </td>
                                    <td> <?=$value["info"];?> </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                <?php endif;?>
            </div>

            <div class="row">
                <?php if(isset($career)): ?>
                    <table class="table table-sm table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Anio</th>
                                <th scope="col">Proyecto lectivo</th>
                                <th scope="col">CodigoUnidadAcademica</th>
                                <th scope="col">CodigoTipoProyectoLectivo</th>
                                <th scope="col">CodigoProyectoLectivo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($career->DevolverTodasProyectosLectivoResult as $key => $value): ?>
                                <tr>
                                    <th scope="row"> <?=$key+1;?> </th>
                                    <td> <?=$value->cAnioProyectoLectivo;?> </td>
                                    <td> <?=$value->cDescripcionProyecto;?> </td>
                                    <td> <?=$value->cCodigoUnidadAcademica;?> </td>
                                    <td> <?=$value->iCodigoTipoProyectoLectivo;?> </td>
                                    <td> <?=$value->iCodigoProyectoLectivo;?> </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                <?php endif;?>
            </div>

            <div class="row">
                <?php if(isset($semester)): ?>
                    <table class="table table-sm table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">UnidadAcademica</th>
                                <th scope="col">Semestre</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($semester->DevolverListaSemetresResult as $key => $value): ?>
                                <tr>
                                    <th scope="row"> <?=$key+1;?> </th>
                                    <td> <?=$value->uniAcademica;?> </td>
                                    <td> <?=$value->numSemestres;?> </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                <?php endif;?>
            </div>

            <div class="row">
                <?php if(isset($course)): ?>
                    <table class="table table-sm table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">CodigoAsignatura</th>
                                <th scope="col">DescripcionAsignatura</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($course as $key => $value): ?>
                                <tr>
                                    <th scope="row"> <?=$key+1;?> </th>
                                    <td> <?=$value->CodigoAsignatura;?> </td>
                                    <td> <?=$value->DescripcionAsignatura;?> </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                <?php endif;?>
            </div>

            <div class="row">
                <?php if(isset($schedule)): ?>
                    <table class="table table-sm table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Codigo</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Sec</th>
                                <th scope="col">Asignatura</th>
                                <th scope="col">Docente</th>
                                <th scope="col">Carrera</th>
                                <th scope="col">Dia</th>
                                <th scope="col">Hora</th>
                                <th scope="col">Matriculados</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($schedule->ObtenerHorariosSemestreCursoResult as $key => $value): ?>
                                <tr>
                                    <th scope="row"> <?=$key+1;?> </th>
                                    <td> <?=$value->CodigoAsignatura;?> </td>
                                    <td> <?=$value->TipoAsignatura;?> </td>
                                    <td> <?=$value->NumeroGrupo;?> </td>
                                    <td> <?=$value->DescripcionAsignatura;?> </td>
                                    <td> <?=$value->NombreDocente;?> </td>
                                    <td> <?=$value->DescripcionPrograma;?> </td>
                                    <td> <?=$value->DiaSemana;?> </td>
                                    <td> <?=$value->Horas;?> </td>
                                    <td> <?=$value->Matriculados;?> </td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                <?php endif;?>
            </div>
        </div>
    </section>
</body>
</html>