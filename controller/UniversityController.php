<?php
/**
 *
 */

class UniversityController
{
    public function getCourseOptimized()
    {
        require_once("model/University.php");
        $university = new University();

        $data = array();

        $career = $university->getCareer();

        $semester = $university->getSemester(53, 288);
        foreach ($career->DevolverTodasProyectosLectivoResult as $value1) {
            foreach ($semester->DevolverListaSemetresResult as $value2) {
                $courseData = $university->getCourseData($value2->uniAcademica, $value2->numSemestres);
                if (!empty($courseData)) {
                    $courseData = array_unique($courseData->ObtenerCursosSemestreProgramaResult, SORT_REGULAR);
                    foreach ($courseData as $value3) {
                        $temp = array(
                            "career" => $value1->cDescripcionProyecto,
                            "semester" => $value2->numSemestres,
                            "course" => $value3->DescripcionAsignatura,
                        );
                        array_push($data, $temp);
                        $schedule = $university->getSchedule($value2->uniAcademica, $value2->numSemestres, $value3->CodigoAsignatura);
                    }
                }
                
            }
        }
        return $data;
    }

    public function getCourse()
    {
        require_once("model/University.php");
        $university = new University();

        $data = array();

        $career = $university->getCareer();
        foreach ($career->DevolverTodasProyectosLectivoResult as $value1) {
            $semester = $university->getSemester($value1->cCodigoUnidadAcademica, $value1->iCodigoTipoProyectoLectivo);
            foreach ($semester->DevolverListaSemetresResult as $value2) {
                $courseData = $university->getCourseData($value2->uniAcademica, $value2->numSemestres);
                if (!empty($courseData)) {
                    $courseData = array_unique($courseData->ObtenerCursosSemestreProgramaResult, SORT_REGULAR);
                    foreach ($courseData as $value3) {
                        $schedule = $university->getSchedule($value2->uniAcademica, $value2->numSemestres, $value3->CodigoAsignatura);
                        foreach ($schedule->ObtenerHorariosSemestreCursoResult as $value4) {
                            $temp = array(
                                    "career" => $value1->cDescripcionProyecto,
                                    "semester" => $value2->numSemestres,
                                    "course" => $value3->DescripcionAsignatura,
                                    "teacher" => $value4->NombreDocente,
                                    "day" => $value4->DiaSemana,
                                    "hour" => $value4->Horas,
                                    "info" => $value4->Matriculados,
                            );
                            array_push($data, $temp);
                        }
                    }
                }
                
            }
        }
        return $data;
    }
    
    public function index()
    {
        require_once("model/University.php");

        $university = new University();
        $data = $university->getCareer();

        //Elimino las carreras de segunda especialidad, etc.
        foreach ($data->DevolverTodasProyectosLectivoResult as $key => $value) {
            if ($value->iCodigoProyectoLectivo > "2065" || $value->iCodigoProyectoLectivo < "2025") {
                unset($data->DevolverTodasProyectosLectivoResult[$key]);
            }
        }

        return $data;
    }
    public function index2()
    {
        require_once("model/University.php");

        $university = new University();
        return $data = $university->getSemester("4J", 288);
    }
    public function index3()
    {
        require_once("model/University.php");

        $university = new University();
        return $data = $university->getCourseData("4J", 10);
    }
    public function index4()
    {
        require_once("model/University.php");

        $university = new University();
        return $data = $university->getSchedule("4J", 10, 5904323);
    }
}
 ?>
